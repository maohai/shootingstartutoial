using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class ProjectDataManager : PersitentSingleton<ProjectDataManager> {
    #region DATA
    [System.Serializable]
    public class CDInfos {
        public string Name;
        public string Singer;
        public string Description;
        public string ReleaseTime;
        public string Company;
        public class Singles {
            public string name;
            public class cd_single_infos {
                public string description;
                public string image_url;
                public float theoretical_value;
            }
        }
        //序列化一个类
        
        //public CDInfos(string description, string image_url, float theoretical_value) {
        //    this.description = description;
        //    this.image_url = image_url;
        //    this.theoretical_value = theoretical_value;
        //}
    }
    [System.Serializable]
    public class Tape {
        //序列化一个类
        public string name;
        public string start_time;
        public string end_time;
        public Tape(string name, string start_time, string end_time) {
            this.name = name;
            this.start_time = start_time;
            this.end_time = end_time;
        }
    }
    [System.Serializable]
    public class BD {
        //序列化一个类
        public int id;
        public string rare_type;
        public float earned_time;
        public BD(int id, string rare_type, float earned_time) {
            this.id = id;
            this.rare_type = rare_type;
            this.earned_time = earned_time;
        }
    }
    [System.Serializable]
    public class MyData {
        //最终要存储JSON的数据格式
        public List<CDInfos> listCD = new List<CDInfos>();
        //public List<Tape> listTape= new List<Tape> ();
        //public List<BD> listBD = new List<BD>();
    }
    readonly string ReadFileName = "Collections.json";
    //public InputField inputField;//输入框
    //public Text contentText;//文本框
    //public void ReadData() {
    //    StreamReader streamreader = new StreamReader(Application.dataPath + "/Project/Collections.json");//读取数据，转换成数据流
    //    JsonReader js = new JsonReader(streamreader);//再转换成json数据
    //    Root r = JsonMapper.ToObject<Root>(js);//读取
    //    for (int i = 0; i < r.TestData.Count; i++)//遍历获取数据
    //    {
    //        if (inputField.text == r.TestData[i].ID) {
    //            contentText.text = "ID号码：" + r.TestData[i].ID + "  类型：" + r.TestData[i].Type + "  名字：" + r.TestData[i].Name;
    //            break;
    //        }
    //    }
    //} 
    public void SaveJsonData() {
        string fileUrl = Application.dataPath + "/Project/Collections.json";
        var json = File.ReadAllText(fileUrl);
        var data = JsonUtility.FromJson<MyData>(json);
        //找到数据修改数据
        foreach (CDInfos item in data.listCD) {
            Debug.Log(item);
        }
    }
    public string ReadJsonData() {
        string readData;
        //获取到路径
        string fileUrl = Application.dataPath + "/Project/Collections.json";
        //读取文件
        using (StreamReader sr = File.OpenText(fileUrl)) {
            //数据保存
            readData = sr.ReadToEnd();
            sr.Close();
        }
        return readData;
    }
    //string playerName = "gou dan";
    //public void SetPlayerName(string newName) => playerName = newName;
    //public void SaveMyDataData() {
    //数据存档
    //var playerScoreData = LoadMyDataData();

    //playerScoreData.list.Add(new MyData(score, playerName));
    //playerScoreData.list.Sort((x, y) => y.score.CompareTo(x.score));//降序

    //SaveSystemTutorial.SaveSystem.SaveByJson(SaveFileName, playerScoreData);
    //}
    //public MyData LoadMyDataData() {
    //    //读取得分列表
    //    var playerScoreData = new MyData();
    //    if (SaveSystemTutorial.SaveSystem.SaveFileExists(SaveFileName))
    //        playerScoreData = SaveSystemTutorial.SaveSystem.LoadFromJson<MyDataData>(SaveFileName);
    //    else {
    //        while (playerScoreData.list.Count < 10)
    //            playerScoreData.list.Add(new MyData(0, playerName));
    //        SaveSystemTutorial.SaveSystem.SaveByJson(SaveFileName, playerScoreData);
    //    }
    //    return playerScoreData;
    //}
    #endregion
    /*获取当前脚本的文件夹路径，参数为脚本的名字*/
    string GetPath(string _scriptName) {
        string[] path = UnityEditor.AssetDatabase.FindAssets(_scriptName);
        if (path.Length > 1) {
            Debug.LogError("有同名文件" + _scriptName + "获取路径失败");
            return null;
        }
        //将字符串中得脚本名字和后缀统统去除掉
        string path1 = AssetDatabase.GUIDToAssetPath(path[0]).Replace((@"/" + _scriptName + ".cs"), "");
        return path1;
    }
    string GetJson(string jsonName) {
        string[] path = UnityEditor.AssetDatabase.FindAssets(jsonName);
        if (path.Length > 1) {
            Debug.LogError("有同名文件" + jsonName + "获取路径失败");
            return null;
        }
        //将字符串中得脚本名字和后缀统统去除掉
        string path1 = AssetDatabase.GUIDToAssetPath(path[0]).Replace((@"/" + jsonName + ".json"), "");
        return path1;
    }

}
