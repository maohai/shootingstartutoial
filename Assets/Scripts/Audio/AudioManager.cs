using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AudioManager : 
    PersitentSingleton<AudioManager>//持久化单例
{
    [SerializeField] AudioSource sFXPlayer;
    const float MIN_PITCH = 0.9f;
    const float MAX_PITCH = 1.1f;
    //适合普通UI音效
    public void PlaySFX(AudioData audioData) {
        //audioClip:音频剪辑，volume：音量
        //sFXPlayer.clip = audioClip;
        //sFXPlayer.volume = volume;
        //sFXPlayer.Play();//无法复数播放音乐，如果再次进入则直接掐断
        //上面代码直接被这一行代码覆盖
        sFXPlayer.PlayOneShot(audioData.audioClip, audioData.volume);//播放新的音频时，不会掐断上一次播放的
    }
    //适合子弹发射音效
    public void PlayRandomSFX(AudioData audioData) {//随机播放，特指不同子弹声音大小、音质等方面的不同
        //修改音高属性
        sFXPlayer.pitch = Random.Range(MIN_PITCH, MAX_PITCH);
        PlaySFX(audioData);
    }
    public void PlayRandomSFX(AudioData[] audioDatas) { //当子弹命中的时候，随机播放其中一个音效
        PlayRandomSFX(audioDatas[Random.Range(0,audioDatas.Length)]);
    }
}

//System.Serializable 会将共有属性暴露在编辑器中
[System.Serializable] public class AudioData{
    public AudioClip audioClip;//音频
    public float volume;//音量
}