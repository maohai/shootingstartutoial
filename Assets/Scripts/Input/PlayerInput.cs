using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

/// <summary>
///
/// </summary>

// 在 Assets 下添加菜单
// fileName 生成名为 Bullet的脚本
// menuName 菜单按钮名New Bullet
// order 按钮显示顺序
//[CreateAssetMenu(fileName = "Bullet", menuName = "New Bullet", order = 1)]
//通过菜单创建指定脚本文件
[CreateAssetMenu(menuName = "Player Input")]

//ScriptableObject主要作用
//1） 编辑模式下的数据持久化
//2） 配置文件 （配置游戏中的数据）
//3） 数据复用 （多个对象共用一套数据）
//InputActions.IGamePlayActions 是继承于InputActions里面的一个类
public class PlayerInput : ScriptableObject, 
    InputActions.IGamePlayActions,
    InputActions.IPauseMenuActions,
    InputActions.IGameOverScreenActions
    {

    //一定要记得是InputActions,少一个s不行
    InputActions inputActions;
    //需要引入UnityEngine.Events
    //UnityAction是一个委托，和system里的action是一样的
    //差别在于类型的名字和引用的命名空间，用UnityAction这样就不用在导入system命名空间了
    public event UnityAction<Vector2>/*接收一个Vector2的参数，下面会用到*/ onMove = delegate {/*给一个空的委托作为初始值，这样事件永远不会为空了*/ };
    public event UnityAction onStopMove = delegate { };
    public event UnityAction onFire = delegate { };
    public event UnityAction onStopFire = delegate { };
    public event UnityAction onDodge = delegate { };
    public event UnityAction onOverdrive = delegate { };
    public event UnityAction onPause = delegate { };
    public event UnityAction onUnpause = delegate { };
    public event UnityAction onLaunchMissile = delegate { };
    public event UnityAction onConfirmGameOver = delegate { };
    private void OnEnable() {
        //当对象被启用并激活状态时此函数被调用。 
        //初始化操作
        inputActions = new InputActions();
        //回调函数，非常重要
        inputActions.GamePlay.SetCallbacks(this);
        inputActions.PauseMenu.SetCallbacks(this);
        inputActions.GameOverScreen.SetCallbacks(this);
    }
    private void OnDisable() {
        //当对象变为不可用或非激活状态时此函数被调用。 
        //调用禁用鼠标操作类
        DisableAllInput();
    }
    void SwitchActionMap(InputActionMap actionMap,bool isUIInput) {
        inputActions.Disable();//禁用所有的表
        actionMap.Enable();//启用当前的表
        if (isUIInput) {
            Cursor.visible = true;//鼠标显示
            Cursor.lockState = CursorLockMode.None;
        }else {
            Cursor.visible = false;//鼠标隐藏
            Cursor.lockState = CursorLockMode.Locked;//鼠标为锁定
        }
    }
    //启用Gameplay表
    public void EnableGameplayInput() => SwitchActionMap(inputActions.GamePlay, false);
    //启用PauseMenu表
    public void EnablePauseMenuInput() => SwitchActionMap(inputActions.PauseMenu, true);
    //切换到动态更新模式
    public void SwitchToDynamicUpdateMode() => InputSystem.settings.updateMode = InputSettings.UpdateMode.ProcessEventsInDynamicUpdate;
    //切换到固定更新模式
    public void SwitchToFixedUpdateMode() => InputSystem.settings.updateMode = InputSettings.UpdateMode.ProcessEventsInFixedUpdate;
    //开启游戏结束动作表
    public void EnableGameOverScreenInput() => SwitchActionMap(inputActions.GameOverScreen, false);

    public void DisableAllInput() {//用于过场动画时，禁用掉所有的鼠标操作
        inputActions.Disable();//禁用所有的表
    }
    public void OnMove(InputAction.CallbackContext context) {
        //当接受的信号为移动信号时 InputActionPhase是一个枚举类型
        //Disabled = 0,禁用时
        //Waiting = 1,被启用时，但是没有响应的信号输入
        //Started = 2,按下按键的那一帧，相当于Input.GetKeyDown()
        //Performed = 3,输入动作已执行的时候，包含了按下未弹起相当于Input.GetKey
        //Canceled = 4,信号停止的时候

        //因为是持续的移动，所以选择Performed
        if (context.performed)
            //读取输入动作的二维向量的值
            onMove.Invoke(context.ReadValue<Vector2>());

        if (context.canceled) 
            onStopMove.Invoke();
    }

    public void OnFire(InputAction.CallbackContext context) {
        if (context.performed)
            onFire.Invoke();
        if (context.canceled)
            onStopFire.Invoke();
    }

    public void OnDodge(InputAction.CallbackContext context) {
        if (context.performed)
            onDodge.Invoke();
    }

    public void OnOverdrive(InputAction.CallbackContext context) {
        if (context.performed)
            onOverdrive.Invoke();
    }

    public void OnPause(InputAction.CallbackContext context) {
        if (context.performed)
            onPause.Invoke();
    }

    public void OnUnpause(InputAction.CallbackContext context) {
        if (context.performed)
            onUnpause.Invoke();
    }
    public void OnLaunchMissile(InputAction.CallbackContext context) {
        if (context.performed)
            onLaunchMissile.Invoke();
    }

    public void OnConfirmGameOver(InputAction.CallbackContext context) {
        if (context.performed)
            onConfirmGameOver.Invoke();
    }
}
