using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem.UI;//InputSystemUIInputModule
using UnityEngine.UI;//Selectable

public class UIInput : Singleton<UIInput> {
    [SerializeField] PlayerInput playerInput;

    InputSystemUIInputModule UIInputModule;
    protected override void Awake() {
        base.Awake();
        UIInputModule = GetComponent<InputSystemUIInputModule>();
        UIInputModule.enabled = false;//禁用UI输入模块
    }
    public void SelectUI(Selectable UIObject) {//Selectable是所有可被选中unity ui类的基类
        UIObject.Select();//选中这个UI
        UIObject.OnSelect(null);//将UI设置成正确的状态
        UIInputModule.enabled = true;//启用UI输入模块
    }
    public void DisableAllUIInputs() {
        playerInput.DisableAllInput();//禁用所有的playerInput输入
        UIInputModule.enabled = false;//禁用本UI的输入
    }
}
