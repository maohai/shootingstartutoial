using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverScreen : MonoBehaviour
{
    [SerializeField] PlayerInput input;
    [SerializeField] Canvas hUDCancas;
    [SerializeField] AudioData confirmGameOverSound; 
    Canvas canvas;
    Animator animator;
    int exitStateID = Animator.StringToHash("GameOverScreenExit");
    private void Awake() {
        canvas = GetComponent<Canvas>();
        animator = GetComponent<Animator>();
        canvas.enabled = false;
        animator.enabled = false;
    }
    private void OnEnable() {
        GameManager.onGameOver += OnGameOver;
        input.onConfirmGameOver += OnConfirmGameOver;
    }
    private void OnDisable() {
        GameManager.onGameOver -= OnGameOver;
        input.onConfirmGameOver -= OnConfirmGameOver;
    }
    void OnGameOver() {
        hUDCancas.enabled = false;//关闭HUD界面
        canvas.enabled = true;//开启游戏结束画面
        animator.enabled = true;//启用动画器播放
        input.DisableAllInput();//禁用掉所有玩家的输入
    }
    void OnConfirmGameOver() {
        AudioManager.Instance.PlaySFX(confirmGameOverSound);//播放确认游戏的音效
        input.DisableAllInput();
        animator.Play(exitStateID);//播放动画
        ScenesLoad.Instance.LoadScoringScene();//通知加载计分场景   TODO
    }
    void EnableGameOverScreenInput() {//实现动作表的切换
        input.EnableGameOverScreenInput();
    }
}
