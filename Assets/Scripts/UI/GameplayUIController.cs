using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameplayUIController : MonoBehaviour
{
    [SerializeField] PlayerInput playerInput;
    [SerializeField] Canvas hUDCanvas;
    [SerializeField] Canvas menusCanvas;
    [SerializeField] Button resumeButton;
    [SerializeField] Button optionsButton;
    [SerializeField] Button mainMenuButton;
    [SerializeField] AudioData pauserSFX;
    [SerializeField] AudioData unpauserSFX;

    int buttonPressedParameterID = Animator.StringToHash("Pressed");
    private void OnEnable() {
        playerInput.onPause += Pause;
        playerInput.onUnpause += UnPause;

        //resumeButton.onClick.AddListener(OnResumeButtonClick);//订阅函数
        //optionsButton.onClick.AddListener(OnOptionsButtonClick);
        //mainMenuButton.onClick.AddListener(OnMainMenuButtonClick);
        //按钮的功能都交给动画处理器来管理，所以订阅退订就不需要了
        ButtonPressedBehavior.buttonFunctionTable.Add(resumeButton.gameObject.name, OnResumeButtonClick);
        ButtonPressedBehavior.buttonFunctionTable.Add(optionsButton.gameObject.name, OnOptionsButtonClick);
        ButtonPressedBehavior.buttonFunctionTable.Add(mainMenuButton.gameObject.name, OnMainMenuButtonClick);
    }
    private void OnDisable() {
        playerInput.onPause -= Pause;
        playerInput.onUnpause -= UnPause;

        //resumeButton.onClick.RemoveAllListeners();//移除所有订阅的函数
        //optionsButton.onClick.RemoveAllListeners();
        //mainMenuButton.onClick.RemoveAllListeners();
        ButtonPressedBehavior.buttonFunctionTable.Clear();//清空字典
    }
    void Pause() {
        //Time.timeScale = 0f;
        hUDCanvas.enabled = false;
        menusCanvas.enabled = true;
        Cursor.visible = false;//鼠标显示隐藏
        Cursor.lockState = CursorLockMode.Locked;//鼠标锁定
        GameManager.GameState = GameState.Paused;
        TimeController.Instance.Pause();
        playerInput.EnablePauseMenuInput();
        playerInput.SwitchToDynamicUpdateMode();
        UIInput.Instance.SelectUI(resumeButton);//当暂停界面出现的时候，就会自动选择回复按钮
        AudioManager.Instance.PlaySFX(pauserSFX);
    }
    public void UnPause() {
        resumeButton.Select();//先选中恢复按钮，再切换到按下状态
        //resumeButton.animator.SetTrigger("Pressed");//相比于传入一个字符串，但是传入一个hash值性能会更好
        resumeButton.animator.SetTrigger(buttonPressedParameterID);
        AudioManager.Instance.PlaySFX(unpauserSFX);
        //OnResumeButtonClick();
    }
    void OnResumeButtonClick() {
        //Time.timeScale = 1f;
        hUDCanvas.enabled = true;
        menusCanvas.enabled = false;
        GameManager.GameState = GameState.Playing;
        TimeController.Instance.Unpause();
        playerInput.EnableGameplayInput();
        playerInput.SwitchToFixedUpdateMode();
    }
    void OnOptionsButtonClick() {
        //以下代码防止游戏卡住
        UIInput.Instance.SelectUI(optionsButton);
        playerInput.EnablePauseMenuInput();
    }
    void OnMainMenuButtonClick() {
        menusCanvas.enabled = false;
        ScenesLoad.Instance.LoadMainMenuScene();

    }
}
