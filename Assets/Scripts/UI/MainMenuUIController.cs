using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MainMenuUIController : MonoBehaviour {
    [Header("CANVAS")]
    [SerializeField] Canvas mainMenuCanvas;
    [Header("=== BUTTONS ===")]
    [SerializeField] Button buttonStart;
    [SerializeField] Button buttonOptions;
    [SerializeField] Button buttonQuit;
    private void OnEnable() {
        ButtonPressedBehavior.buttonFunctionTable.Add(buttonStart.gameObject.name, OnButtonStartClicked);
        ButtonPressedBehavior.buttonFunctionTable.Add(buttonOptions.gameObject.name, OnButtonOptionsClicked);
        ButtonPressedBehavior.buttonFunctionTable.Add(buttonQuit.gameObject.name, OnButtonQuitClicked);
    }
    private void OnDisable() {
        ButtonPressedBehavior.buttonFunctionTable.Clear();//清空字典
    }
    private void Start() {
        Time.timeScale = 1f;//当从菜单会到主界面的的时候，时间刻度是0，所以恢复为1
        GameManager.GameState = GameState.Playing;
        UIInput.Instance.SelectUI(buttonStart);
    }
    void OnButtonStartClicked() {
        mainMenuCanvas.enabled = false;
        ScenesLoad.Instance.LoadGameplayScene();
    }
    void OnButtonOptionsClicked() {
        UIInput.Instance.SelectUI(buttonOptions);
    }
    void OnButtonQuitClicked() {
        //如果是在unity编译器运行，就会退出游戏界面，否则退出游戏
    #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
    #else
        Application.Quit();
    #endif
    }

}
