using UnityEngine;
using UnityEngine.UI;

public class WaveUI : MonoBehaviour
{
    Text waveText;

    private void Awake() {
        GetComponent<Canvas>().worldCamera = Camera.main;//设置画布模式为World，然后复制主摄像机
        waveText = GetComponentInChildren<Text>();
    }
    private void OnEnable() {//脚本启用时
        waveText.text = "- WAVE " + EnemyManager.Instance.WaveNumber + " -";
    }
}
