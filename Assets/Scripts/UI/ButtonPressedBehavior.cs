using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonPressedBehavior : StateMachineBehaviour
{
    //储存按钮的字典，第二个参数为不带参数的委托
    public static Dictionary<string, System.Action> buttonFunctionTable;

    private void Awake() {
        buttonFunctionTable = new Dictionary<string, System.Action>();
    }
    // 状态进入函数
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        UIInput.Instance.DisableAllUIInputs();//当按钮按下的时候，直接禁用所有的输入
    }
    //状态更新函数
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

    //}
    // 状态退出函数
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        //Animator animator为脚本所挂载的动画
        buttonFunctionTable[animator.gameObject.name].Invoke();//执行
    }   
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
