using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StateBar_HUD : StateBar
{
    [SerializeField] protected Text percentText;
    protected virtual void SetPersentText() {
        //浮点数去小数
        percentText.text =targetFillAmount.ToString("P0");
    }
    public override void Initialize(float cuttentValue, float maxValue) {
        base.Initialize(cuttentValue, maxValue);
        SetPersentText();//初始化后设置百分比
    }
    protected override IEnumerator BufferedFillCoroutine(Image image) {
        SetPersentText();//修改前设置百分比
        return base.BufferedFillCoroutine(image);
    }
}
