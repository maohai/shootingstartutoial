using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;//检测鼠标光标的行为

public class UIEventTrigger : MonoBehaviour, 
    IPointerEnterHandler,
    IPointerDownHandler,
    ISelectHandler,
    ISubmitHandler 
    {
    [SerializeField] AudioData selectSFX;
    [SerializeField] AudioData submitSFX;

    //鼠标是否停留在脚本挂载的对象上
    public void OnPointerEnter(PointerEventData eventData) {
        AudioManager.Instance.PlaySFX(submitSFX);
    }
   //鼠标是否离开
    public void OnPointerDown(PointerEventData eventData) {
        AudioManager.Instance.PlaySFX(selectSFX);
    }
    //鼠标选中
    public void OnSelect(BaseEventData eventData) {
        AudioManager.Instance.PlaySFX(submitSFX);
    }
    //鼠标提交
    public void OnSubmit(BaseEventData eventData) {
        AudioManager.Instance.PlaySFX(submitSFX);
    }
}
