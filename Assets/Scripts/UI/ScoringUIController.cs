using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ScoringUIController : MonoBehaviour
{
    [SerializeField] Image background;
    [SerializeField] Sprite[] backgroundImages;
    [Header("=== SCORINGSCREEN ===")]
    [SerializeField] Canvas scoringScreenCanvas;
    [SerializeField] Text playerScoreText;
    [SerializeField] Button buttonMainMenu;
    [SerializeField] Transform highScoreLeaderboardContainer;
    [Header("=== HIGH SCORE SCREEN ====")]
    [SerializeField] Canvas newHighScoreScreenCanvas;
    [SerializeField] Button buttonCancel;
    [SerializeField] Button buttonSubmit;
    [SerializeField] InputField playerNameInputField;//玩家名字输入框

    private void Start() {
        Cursor.visible = true;//显示光标
        Cursor.lockState = CursorLockMode.None;//无锁定

        ShowRandomBackground();
        if (ScoreManager.Instance.HasNewHighScore)
            ShowNewHighScoreScreen();
        else ShowScoringScreen();//显示积分画面
        ButtonPressedBehavior.buttonFunctionTable.Add(buttonMainMenu.gameObject.name, OnButtonMainMenuClicked);
        ButtonPressedBehavior.buttonFunctionTable.Add(buttonSubmit.gameObject.name, OnButtonSubmitClicked);
        ButtonPressedBehavior.buttonFunctionTable.Add(buttonCancel.gameObject.name, HideNewHighScoreScreen);
        GameManager.GameState = GameState.Scoring;//更改游戏状态
    }

    private void ShowNewHighScoreScreen() {
        newHighScoreScreenCanvas.enabled = true;
        UIInput.Instance.SelectUI(buttonCancel);
    }

    private void OnDisable() {
        newHighScoreScreenCanvas.enabled = true;//开启画布
        UIInput.Instance.SelectUI(buttonCancel);
    }
    private void ShowRandomBackground() {
        background.sprite = backgroundImages[Random.Range(0, backgroundImages.Length)];
    }
    void ShowScoringScreen() {
        scoringScreenCanvas.enabled = true;
        playerScoreText.text = ScoreManager.Instance.Score.ToString();//显示分数
        UIInput.Instance.SelectUI(buttonMainMenu);//默认选择按钮
        //TODO 显示高分排行榜
        UpdataHighScoreLeaderboard();

    }
    void UpdataHighScoreLeaderboard() {

        var playerScoreList = ScoreManager.Instance.LoadPlayerScoreData().list;
        for (int i = 0; i < highScoreLeaderboardContainer.childCount; i++) {
            var child = highScoreLeaderboardContainer.GetChild(i);

            child.Find("Rank").GetComponent<Text>().text = (i + 1).ToString();
            child.Find("Score").GetComponent<Text>().text = playerScoreList[i].score.ToString();
            child.Find("Name").GetComponent<Text>().text = playerScoreList[i].playerName;
        }
    }
    void OnButtonMainMenuClicked() {
        scoringScreenCanvas.enabled = false;
        ScenesLoad.Instance.LoadMainMenuScene();
    }
    void OnButtonSubmitClicked() {
        if (!string.IsNullOrEmpty(playerNameInputField.text))
            ScoreManager.Instance.SetPlayerName(playerNameInputField.text);//修改名字
        HideNewHighScoreScreen();
    }
    private void HideNewHighScoreScreen() {
        newHighScoreScreenCanvas.enabled = false;//隐藏UI画面
        ScoreManager.Instance.SavePlayerScoreData();//保存分数
        ShowRandomBackground();//显示随机背景
        ShowScoringScreen();//显示积分画面
    }
}
