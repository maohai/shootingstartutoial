using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class StateBar : MonoBehaviour
{
    float currentFillAmount;//图片的当前填充值
    protected float targetFillAmount;//图片的目标填充值
    [SerializeField] Image fillImageBack;//前填充图片
    [SerializeField] Image fillImageFront;//后填充图片
    [SerializeField] float fillSpeed = 0.1f;//默认填充速度
    [SerializeField] bool delayFill = true;//是否开启延迟填充
    [SerializeField] float fillDelay = 0.5f;//延迟时间
    Canvas canvas;//画布

    WaitForSeconds waitForDelayFill;//等待填充时间
    Coroutine bufferedFillingCoroutine;
    private void Awake() {
        if(TryGetComponent<Canvas>(out Canvas canvas))
            canvas.worldCamera = Camera.main;//画布模式设置为主摄像机 
        waitForDelayFill = new WaitForSeconds(fillDelay);

    }
    private void OnDisable() {
        StopAllCoroutines();//直接停止所有的协程
    }
    public virtual void Initialize(float currentValue,float maxValue) {
        currentFillAmount = currentValue / maxValue;//百分比
        targetFillAmount = currentFillAmount;
        fillImageBack.fillAmount = currentFillAmount;
        fillImageFront.fillAmount = currentFillAmount;
    }
    public void UpdateState(float cuttentValue, float maxValue) {
        targetFillAmount = cuttentValue / maxValue;//更新目标值
        if (bufferedFillingCoroutine != null)
            StopCoroutine(bufferedFillingCoroutine);

        if (currentFillAmount > targetFillAmount) {//状态减少
            fillImageFront.fillAmount = targetFillAmount;//前面图片的填充值=目标填充值
            bufferedFillingCoroutine =  //慢慢减少后面的图片的填充值
                StartCoroutine(BufferedFillCoroutine(fillImageBack));
        }
        else if (currentFillAmount < targetFillAmount) {//状态增加   
            fillImageBack.fillAmount = targetFillAmount;//后面图片的填充值=目标填充值
            bufferedFillingCoroutine =  //慢慢增加前面的图片的填充值
                StartCoroutine(BufferedFillCoroutine(fillImageFront));
        }
    }
    float t;
    float previousFillAmount;
    protected virtual IEnumerator BufferedFillCoroutine(Image image) {//状态缓慢填充
        if (delayFill)
            yield return waitForDelayFill;//等待一段时间

        t = 0;
        previousFillAmount = currentFillAmount;
        while (t < 1) {
            t += fillSpeed * Time.deltaTime;
            //插值，当前值，目标值，时间
            currentFillAmount = Mathf.Lerp(previousFillAmount, targetFillAmount, t);
            image.fillAmount = currentFillAmount;
            yield return null;
        }
    }
}
