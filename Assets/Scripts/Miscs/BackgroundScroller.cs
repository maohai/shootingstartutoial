using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///
/// </summary>

public class BackgroundScroller : MonoBehaviour
{
    //暴露变量，而且依然是私有
    [SerializeField] Vector2 scrollVelocity;
    Material material;
    private void Awake() {
        material = GetComponent<Renderer>().material;
    }
    IEnumerator Start() {
        while(GameManager.GameState != GameState.GameOver) {
            //页面滚动，改变Offset值
            material.mainTextureOffset += scrollVelocity * Time.deltaTime;

            yield return null;
        }

    }
}
