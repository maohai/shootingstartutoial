using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPowerUp : LootItem {
    [SerializeField] int fullWeaponScoreBonus = 200;
    [SerializeField] AudioData fullWeaponPickUpSFX;

    protected override void PickUp() {
        if (player.IsFullPower) {
            pickUpSFX = fullWeaponPickUpSFX;
            lootMessage.text = $"SCORE + {fullWeaponScoreBonus}";
            ScoreManager.Instance.AddScore(fullWeaponScoreBonus);
        }
        else {
            pickUpSFX = defaultPickUpSFX;
            lootMessage.text = "POWER UP";
            player.PowerUp();
        }
        base.PickUp();
    }
}
