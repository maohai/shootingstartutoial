using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LootItem : MonoBehaviour
{
    [SerializeField] float minSpeed = 5f;
    [SerializeField] float maxSoeed = 15f;
    [SerializeField] protected AudioData defaultPickUpSFX;

    protected Player player;
    protected Text lootMessage;
    int pickUpStateID = Animator.StringToHash("PickUp");
    Animator animator;
    protected AudioData pickUpSFX;
    private void Awake() {
        player = FindObjectOfType<Player>();
        animator = GetComponent<Animator>();
        lootMessage = GetComponentInChildren<Text>(true);//参数为true就可以获取到禁用的组件了
        pickUpSFX = defaultPickUpSFX;
    }
    private void OnEnable() {
        StartCoroutine(nameof(MoveCoroutine));
    }
    private void OnTriggerEnter2D(Collider2D collision) {
        PickUp();
    }
    protected virtual void PickUp() {
        StopAllCoroutines();
        animator.Play(pickUpStateID);
        AudioManager.Instance.PlayRandomSFX(pickUpSFX);
    }
    IEnumerator MoveCoroutine() {
        float speed = Random.Range(minSpeed, maxSoeed);
        Vector3 direction = Vector3.left;
        while (true) {
            if (player.isActiveAndEnabled)
                direction = (player.transform.position - transform.position).normalized;
            transform.Translate(direction * speed * Time.deltaTime);
            yield return null;
        }
    }
}
