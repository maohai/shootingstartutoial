using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable] public class LootSetting
{
    public GameObject prefab;
    [Range(0, 100f)] public float dropPercentage;
    public void Spawn(Vector3 pos) {
        if(Random.Range(0,100f)<=dropPercentage) {
            PoolManager.Release(prefab, pos);
        }
    }
}
