using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerEnergy : Singleton<PlayerEnergy>//泛型单例
{
    [SerializeField] EnergyBar energyBar;
    [SerializeField] float overdriveInterval = 0.1f;//消耗一点能量值所需要的时间

    public const int MAX = 100;
    public const int PERSENT = 1;
    int energy = 0;
    bool available = true;

    WaitForSeconds waitForOverdriveInterval;
    protected override void Awake() {
        base.Awake();
        waitForOverdriveInterval = new WaitForSeconds(overdriveInterval);
    }
    private void OnEnable() {
        PlayerOverdrive.on += PlayerOverdriveOn;
        PlayerOverdrive.off += PlayerOverdriveOff;
    }
    private void OnDisable() {
        PlayerOverdrive.on -= PlayerOverdriveOn;
        PlayerOverdrive.off -= PlayerOverdriveOff;
    }
    private void Start() {
        energyBar.Initialize(energy, MAX);
        Obtain(MAX);//开局能量全满
    }
    public void Obtain(int value) {//获取能量
        if (energy == MAX || !available || !gameObject.activeSelf) return;
        energy = Mathf.Clamp(energy + value, 0, MAX);
        energyBar.UpdateState(energy, MAX);
    }
    public void Use(int value) {//使用
        energy -= value;
        energyBar.UpdateState(energy, MAX);
        if (energy == 0 && !available)
            PlayerOverdrive.off.Invoke();
    }
    public bool IsEnough(int value) => energy >= value;//lambda表达式
    void PlayerOverdriveOn() {
        available = false;
        StartCoroutine(nameof(KeepUsingCoroutine));
    }
    void PlayerOverdriveOff() {
        available = true;
        StopCoroutine(nameof(KeepUsingCoroutine));
    }
    IEnumerator KeepUsingCoroutine() {
        while(gameObject.activeSelf && energy > 0) {
            yield return waitForOverdriveInterval;
            Use(PERSENT);
        }
    }
}
