using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;//通过委托来减少类与类之间的耦合
public class PlayerOverdrive : MonoBehaviour {
    public static UnityAction on = delegate { };//打开
    public static UnityAction off = delegate { };//关闭

    [SerializeField] GameObject triggerVFX;
    [SerializeField] GameObject engineVFXNormal;
    [SerializeField] GameObject engineVFXOverdrive;
    [SerializeField] AudioData onSFX;
    [SerializeField] AudioData offSFX;
    private void Awake() {
        on += On;
        off += Off;
    }
    private void OnDestroy() {
        on -= On;
        off -= Off;
    }
    void On() {
        triggerVFX.SetActive(true);//开启视觉特效  AutoDeactivate脚本会3秒消失
        engineVFXNormal.SetActive(false);
        engineVFXOverdrive.SetActive(true);//开启引擎粒子特效
        AudioManager.Instance.PlayRandomSFX(onSFX);//开启音效
    }
    void Off() {
        engineVFXNormal.SetActive(true);
        engineVFXOverdrive.SetActive(false);//开启引擎粒子特效
        AudioManager.Instance.PlayRandomSFX(offSFX);//开启音效
    }
}
