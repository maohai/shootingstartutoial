using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//脚本添加刚体
[RequireComponent(typeof(Rigidbody2D))]
public class Player : Character {
    [SerializeField] StateBar_HUD stateBar_HUD;//血条脚本
    [SerializeField] bool regenerathHeath = true;//是否打开生命值自动回复
    [SerializeField] float healthRegenerateTime;//生命值回复时间
    [SerializeField,Range(0,1)] float healthRegeneratePercent;//生命值回复百分比
    //公开，别的类无法访问
    [Header("---- INPUT ----")]
    [SerializeField] PlayerInput input;//输入信号
    [Header("---- MOVE ----")]
    [SerializeField] float moveSpeed = 10f;//移动速度
    //[SerializeField] float paddingX = 0.8f;//测量中心轴和机头的长度
    //[SerializeField] float paddingY = 0.22f;//同上
    [SerializeField] float accelerationTime = 2f;//加速时间
    [SerializeField] float decelerationTime = 2f;//减速时间
    [SerializeField] float moveRotationAngle = 30;//旋转角度
    [Header("---- FIRE ----")]
    [SerializeField] GameObject projectile1;//子弹1预制件
    [SerializeField] GameObject projectile2;//子弹2预制件
    [SerializeField] GameObject projectile3;//子弹3预制件
    [SerializeField] GameObject projectileOverdrive;//能量爆发子弹预制件
    [SerializeField] Transform muzzleMiddle;//子弹发射位置
    [SerializeField] Transform muzzleTop;   //子弹发射位置
    [SerializeField] Transform muzzleBottom;//子弹发射位置
    [SerializeField] ParticleSystem muzzleVFX;//子弹特效
    [SerializeField] float fireInteral = 0.12f;//子弹发射间隔
    [SerializeField, Range(0, 2)] int weaponPower = 0;//子弹威力 
    [SerializeField] AudioData projectileLaunchSFX;//子弹发射音效
    MissileSystem missile;//导弹实例
    [Header("---- DODGE ----")]
    [SerializeField,Range(0,100)] int dodgeEnergyCost = 25;//闪避消耗能量
    new Collider2D collider;//用于闪避，因为collider名字已经被占用了，所以用new直接整成新的
    float currentRoll;//当前翻滚角度
    bool isDodge = false;//是否正在翻滚
    [SerializeField] float maxRoll = 720f;//闪避翻滚两圈
    [SerializeField] float rollSpeed = 360f;//滚转角，也就是一秒360度，那么两圈就是两秒
    [SerializeField] Vector3 dodgeScale = new Vector3(0.5f, 0.5f, 0.5f);//翻滚缩放
    float dodgeDuration;//闪避持续时间
    [SerializeField] AudioData dodgeSFX;//闪避音效
    [Header("---- OVERDRIVE ----")]
    bool isOverdriving = false;//是否正在能量爆发
    [SerializeField] int overdriveDodgeFactor = 2;//处于能量爆发时候能量的消耗
    [SerializeField] float overdriveSpeedFactor = 1.2f;//处于能量爆发时候的速度
    [SerializeField] float overdriveFireFactor = 1.2f;//处于能量爆发时候的火力
    readonly float slowMotionDuration = 1f;

    float paddingX;
    float paddingY;
    readonly float InvincibleTime = 1f;//无敌时间

    Vector2 moveDirection;//移动方向
    Rigidbody2D rigibody;//刚体，用于运动
    Coroutine moveCoroutine;//协程
    Coroutine healthRegenerateCoroutine;//协程
    WaitForSeconds waitForFireInteral;//等待开火时间
    WaitForSeconds waitForOverdriveFireInteral;//能量爆发等待开火时间
    WaitForSeconds waitHealthRegenerateTime;//等待生命值再生时间
    WaitForSeconds waitDecelerationTime;//等待减速时间
    WaitForSeconds waitInvinbleTime;

    #region PROPERTIES
    public bool IsFullHealth => health == maxHealth;
    public bool IsFullPower => weaponPower == 2;

    #endregion

    #region OTHER FUNCTIONS
    private void Awake() {
        rigibody = GetComponent<Rigidbody2D>();
        collider = GetComponent<Collider2D>();
        missile = GetComponent<MissileSystem>();

        var size = transform.GetChild(0).GetComponent<Renderer>().bounds.size;//模型大小
        paddingX = size.x / 2f;
        paddingY = size.y / 2f;

        rigibody.gravityScale = 0f;//重力设置为0
        waitForFireInteral = new WaitForSeconds(fireInteral);//开火间隔
        waitForOverdriveFireInteral = new WaitForSeconds(fireInteral / overdriveFireFactor);//能量爆发开火间隔
        waitHealthRegenerateTime = new WaitForSeconds(healthRegenerateTime);//翻滚时间
        waitDecelerationTime = new WaitForSeconds(decelerationTime);//等待减速时间
        dodgeDuration = maxRoll / rollSpeed;//翻滚时间
        waitInvinbleTime = new WaitForSeconds(InvincibleTime);//无敌时间
    }
    private void Start() {
        input.EnableGameplayInput(); //当玩家移动的时候，GamePlay动作表就会被激活

        stateBar_HUD.Initialize(health,maxHealth);//血条初始化
    }

    public override void TakeDamage(float damage) {//重写继承的TakeDamege函数
        base.TakeDamage(damage);
        PowerDown();
        stateBar_HUD.UpdateState(health,maxHealth);//更新血条
        TimeController.Instance.BulletTime(slowMotionDuration);//子弹时间
        if (gameObject.activeSelf) {
            Move(moveDirection);//即使玩家速度被子弹抵消了，玩家依然可以按照这个速度移动
            StartCoroutine(nameof(InvincibleCoroutine));
            if (regenerathHeath) {
                if (healthRegenerateCoroutine != null)
                    StopCoroutine(healthRegenerateCoroutine);//停用协程
                //继承的协程
                healthRegenerateCoroutine =
                    StartCoroutine(HeathRegenerateCoroutine(waitHealthRegenerateTime, healthRegeneratePercent));
            }
        }
    }
    protected override void OnEnable() {
        base.OnEnable();//调用基类的函数
        //订阅事件
        input.onMove += Move;
        input.onStopMove += StopMove;
        input.onFire += Fire;
        input.onStopFire += StopFire;
        input.onDodge += Dodge;
        input.onOverdrive += Overdrive;
        input.onLaunchMissile += LaunchMissile;

        PlayerOverdrive.on += OverdriveOn;
        PlayerOverdrive.off += OverdriveOff;

    }
    private void OnDisable() {
        //删除事件
        input.onMove -= Move;
        input.onStopMove -= StopMove;
        input.onFire -= Fire;
        input.onStopFire -= StopFire;
        input.onDodge -= Dodge;
        input.onOverdrive -= Overdrive;
        input.onLaunchMissile -= LaunchMissile;

        PlayerOverdrive.on -= OverdriveOn;
        PlayerOverdrive.off -= OverdriveOff;

    }
    public override void RestoreHealth(float value) {
        base.RestoreHealth(value);
        stateBar_HUD.UpdateState(health, maxHealth);//更新血条
    }
    public override void Die() {
        GameManager.onGameOver?.Invoke();
        GameManager.GameState = GameState.GameOver;
        stateBar_HUD.UpdateState(0, maxHealth);//更新血条
        base.Die();
    }
    IEnumerator InvincibleCoroutine() {
        collider.isTrigger = true;
        yield return waitInvinbleTime;
        collider.isTrigger = false;
    }
    #endregion

    #region  MOVE

    //因为OnMove信号声明的是一个接受Vector参数的信号
    void Move(Vector2 moveInput) {
        //moveInput就是当onMove事件被调用时，他讲等于输入信号二维向量的那个值
        //对刚体速度进行操作，赋予物体一个持续不变的速度
        //velocity单位是米每秒
        //rigibody.velocity = moveInput * moveSpeed;
        //加速
        if (moveCoroutine != null)
            StopCoroutine(moveCoroutine);//停止减速
        moveDirection = moveInput.normalized;
        //因为moveInput.y在1,-1之间，moveRotationAngle*moveInput.y会产生不同方向的选装了
        Quaternion moveRotation = Quaternion.AngleAxis(moveRotationAngle * moveInput.y, Vector3.right/*代表X轴*/);
        moveCoroutine = StartCoroutine(MoveCoroutine(accelerationTime, moveDirection * moveSpeed, moveRotation));
        StopCoroutine(nameof(DecelerationCoroutine));
        //开启协程
        StartCoroutine(nameof(MoveRangeLimatationCoroutine));
    }
    void StopMove() {
        //rigibody.velocity = Vector2.zero;
        //减速
        if (moveCoroutine != null)
            StopCoroutine(moveCoroutine);//停止加速
        moveDirection = Vector2.zero;
        //Quaternion.identity 0000
        moveCoroutine = StartCoroutine(MoveCoroutine(decelerationTime, moveDirection, Quaternion.identity));
        //玩家停止移动的时候启用这个协程
        StartCoroutine(nameof(DecelerationCoroutine));
    }
    float t;
    Vector2 previousVelocity;
    Quaternion previousRotation;
    WaitForFixedUpdate waitForFixedUpdate = new WaitForFixedUpdate();
    private IEnumerator MoveCoroutine(float time, Vector2 moveVelocity, Quaternion moveRotation) {
        t = 0f;
        previousVelocity = rigibody.velocity;
        previousRotation = transform.rotation;
        //当t的值小于1时候，循环执行
        while (t < 1f) {
            t += Time.fixedDeltaTime / time;
            //玩家固定时间变化
            rigibody.velocity = Vector2.Lerp(previousVelocity, moveVelocity, t);
            transform.rotation = Quaternion.Lerp(previousRotation, moveRotation, t);
            Debug.Log(t);
            yield return waitForFixedUpdate;
        }
    }
    private IEnumerator MoveRangeLimatationCoroutine() {
        while (true) {
            //限制玩家的范围
            transform.position = Viewport.Instance.PlayerMoveablePosition(transform.position, paddingX, paddingY);
            yield return null;//下一帧继续
        }
    }
    IEnumerator DecelerationCoroutine() {
        yield return waitDecelerationTime;
        StopCoroutine(nameof(MoveRangeLimatationCoroutine));
    }
    #endregion

    #region FIRE
    void Fire() {
        muzzleVFX.Play();
        StartCoroutine(nameof(FireCoroutine));
    }
    void StopFire() {
        muzzleVFX.Stop();
        //使用这个重载是因为 StopCoroutine(FireCoroutine());可能会不执行
        StopCoroutine(nameof(FireCoroutine));
    }
    private IEnumerator FireCoroutine() {
        //while (true) {
        //    switch (weaponPower) {
        //        case 0:
        //            //子弹预制件，位置，不需要旋转
        //            Instantiate(projectile1, muzzleMiddle.position, Quaternion.identity);
        //            break;
        //        case 1:
        //            Instantiate(projectile1, muzzleTop.position, Quaternion.identity);
        //            Instantiate(projectile1, muzzleBottom.position, Quaternion.identity);
        //            break;
        //        case 2:
        //            Instantiate(projectile1, muzzleMiddle.position, Quaternion.identity);
        //            Instantiate(projectile2, muzzleTop.position, Quaternion.identity);
        //            Instantiate(projectile3, muzzleBottom.position, Quaternion.identity);
        //            break;
        //        default:
        //            break;
        //    }
        while (true) {
            switch (weaponPower) {
                case 0:
                    //子弹预制件，位置，不需要旋转
                    PoolManager.Release(isOverdriving?projectileOverdrive:projectile1, muzzleMiddle.position);
                    break;
                case 1:
                    PoolManager.Release(isOverdriving ? projectileOverdrive : projectile1, muzzleTop.position);
                    PoolManager.Release(isOverdriving ? projectileOverdrive : projectile1, muzzleBottom.position);
                    break;
                case 2:
                    PoolManager.Release(isOverdriving ? projectileOverdrive : projectile1, muzzleMiddle.position);
                    PoolManager.Release(isOverdriving ? projectileOverdrive : projectile2, muzzleTop.position);
                    PoolManager.Release(isOverdriving ? projectileOverdrive : projectile3, muzzleBottom.position);
                    break;
                default:
                    break;
            }
            AudioManager.Instance.PlayRandomSFX(projectileLaunchSFX);//播放音效

            //最好不要在while里面new
            //看是否处于能量爆发状态
            yield return isOverdriving ? waitForOverdriveFireInteral : waitForFireInteral;
        }
    }
    #endregion

    #region DODGE

    private void Dodge() {//闪避
        //当正在翻滚或者能量值不够的时候直接返回
        if (isDodge || !PlayerEnergy.Instance.IsEnough(dodgeEnergyCost)) return;
        StartCoroutine(nameof(DodgeCoroutine));
    }
    IEnumerator DodgeCoroutine() {
        isDodge = true;
        AudioManager.Instance.PlayRandomSFX(dodgeSFX);

        //消耗能量
        PlayerEnergy.Instance.Use(dodgeEnergyCost);

        //闪避无敌 直接开关碰撞体
        collider.isTrigger = true;//触发器打开
        currentRoll = 0f;//重置旋转角
        Vector3 currentScale = transform.localScale;//当前旋转值
        TimeController.Instance.BulletTime(slowMotionDuration, slowMotionDuration);//子弹时间
        while (currentRoll < maxRoll) {
            currentRoll += rollSpeed * Time.deltaTime;
            //沿X旋转
            transform.rotation = Quaternion.AngleAxis(currentRoll, Vector3.right);//按X轴来旋转
            #region 注释的缩放代码，被下面一行代码代替
            //当翻滚到最大角的时候再放大
            //if(currentRoll <maxRoll / 2f) {
            //    //currentScale -= (Time.deltaTime / dodgeDuration) * Vector3.one;
            //    //分开赋值,因为无法限制三维向量，所以只能限制各个轴的值来达到限制三维向量的目的
            //    currentScale.x = Mathf.Clamp(currentScale.x - Time.deltaTime / dodgeDuration,dodgeScale.x,1f);
            //    currentScale.y = Mathf.Clamp(currentScale.y - Time.deltaTime / dodgeDuration,dodgeScale.y,1f);
            //    currentScale.z = Mathf.Clamp(currentScale.z - Time.deltaTime / dodgeDuration,dodgeScale.z,1f);
            //}
            //else {
            //    //currentScale += (Time.deltaTime / dodgeDuration) * Vector3.one;
            //    currentScale.x = Mathf.Clamp(currentScale.x + Time.deltaTime / dodgeDuration, dodgeScale.x, 1f);
            //    currentScale.y = Mathf.Clamp(currentScale.y + Time.deltaTime / dodgeDuration, dodgeScale.y, 1f);
            //    currentScale.z = Mathf.Clamp(currentScale.z + Time.deltaTime / dodgeDuration, dodgeScale.z, 1f);
            //}
            //transform.localScale = currentScale;
            #endregion
            //贝塞尔曲线更为光滑
            transform.localScale = QuadraticPoint(Vector3.one,Vector3.one,dodgeScale,currentRoll/maxRoll);

            yield return null;
        }
        collider.isTrigger = false;//触发器关闭
        isDodge = false;

    }
    private Vector3 QuadraticPoint(Vector3 startPoint,Vector3 endPoint,Vector3 controlPoint,float by) {
        return Vector3.Lerp(
            Vector3.Lerp(startPoint, controlPoint, by),
            Vector3.Lerp(controlPoint, endPoint, by),
            by);
    }
    #endregion

    #region OVERDRIVE
    void Overdrive() {
        //当能量为满的时候可以开启能量爆发
        if (!PlayerEnergy.Instance.IsEnough(PlayerEnergy.MAX)) return;
        PlayerOverdrive.on.Invoke();//开启委托
    }
    void OverdriveOn() {
        isOverdriving = true;
        dodgeEnergyCost *= overdriveDodgeFactor;
        moveSpeed *= overdriveSpeedFactor;
        TimeController.Instance.BulletTime(slowMotionDuration, slowMotionDuration);//子弹时间
    }
    void OverdriveOff() {
        isOverdriving = false;
        dodgeEnergyCost /= overdriveDodgeFactor;
        moveSpeed /= overdriveSpeedFactor;


    }
    #endregion

    #region LAUNCHMISSILE
    void LaunchMissile() {
        missile.Launch(muzzleMiddle);
    }
    public void PickUpMissile() {
        missile.PickUp();
    }
    #endregion
    #region POWER UP
    public void PowerUp() {
        weaponPower = Mathf.Min(weaponPower + 1, 2);
    }
    void PowerDown() {
        //写法1
        //weaponPower--;
        //weaponPower = Mathf.Clamp(weaponPower, 0, 2);
        //写法2
        //weaponPower = Mathf.Max(weaponPower - 1, 0);
        //写法3
        //weaponPower = Mathf.Clamp(weaponPower, --weaponPower, 0);
        //写法4
        weaponPower = Mathf.Max(--weaponPower, 0);
    }

    #endregion
}
