using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    [Header("---- DEATH ----")]
    [SerializeField] GameObject deathFVX;//死亡视觉特效
    [SerializeField] AudioData[] deathSFX;//死亡爆炸音效
    [Header("---- HEALTH ----")]
    [SerializeField] protected float maxHealth;//序列化，而且能继承
    protected float health;

    [SerializeField] StateBar onHeadHealthBar;//头顶血条
    [SerializeField] bool showOnHeadHealthBar = true;//是否显示血条

    //OnEnable可被重写
    protected virtual void OnEnable() {
        health = maxHealth;

        if (showOnHeadHealthBar)
            ShowOnHeadHealthBar();
        else HideOnheadHealthBar();
    }
    public void ShowOnHeadHealthBar() {
        onHeadHealthBar.gameObject.SetActive(true);//开启血条
        onHeadHealthBar.Initialize(health, maxHealth);//初始化血条
    }
    public void HideOnheadHealthBar() {
        onHeadHealthBar.gameObject.SetActive(false);
    }
    public virtual void TakeDamage(float damage) {
        if (health == 0) return;//防止出现奇怪的状况

        health -= damage;

        //当启用头顶血条，而且对象还在活跃的时候调用
        if (showOnHeadHealthBar)
            onHeadHealthBar.UpdateState(health, maxHealth);

        if (health <= 0)
            Die();
    }
    public virtual void Die() {

        health = 0f;//血条显示
        AudioManager.Instance.PlayRandomSFX(deathSFX);//死亡爆炸音效
        //由对象池系统来管理这个爆炸特效
        PoolManager.Release(deathFVX,transform.position);
        gameObject.SetActive(false);//死亡直接禁用
    }
    public virtual void RestoreHealth(float value) {//回复血量
        if (health == maxHealth) return;
        health = Mathf.Clamp(health + value, 0, maxHealth);

        if (showOnHeadHealthBar)
            onHeadHealthBar.UpdateState(health, maxHealth);
    }
    protected IEnumerator HeathRegenerateCoroutine(WaitForSeconds waitTime,float percent) {//自动回血功能，按百分比
        //可被子类调用
        while (health < maxHealth) {
            yield return waitTime;
            RestoreHealth(maxHealth * percent);
        }
    }
    protected IEnumerator DamageOverTimeCoroutine(WaitForSeconds waitTime, float percent) {//持续受伤
        //可被子类调用
        while (health > 0f) {
            yield return waitTime;
            RestoreHealth(maxHealth * percent);
        }
    }
}
