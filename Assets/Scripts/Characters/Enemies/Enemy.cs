using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Character
{
    [SerializeField] int scorePoint = 100;
    [SerializeField] int deathEnergyBonus = 3;
    [SerializeField] protected int healthFactor = 2;//小怪每波增加的血量

    LootSpawner lootSpawner;
    protected virtual void Awake() {
        lootSpawner = GetComponent<LootSpawner>();
    }
    protected override void OnEnable() {
        SetHealth();
        base.OnEnable();
    }
    protected virtual void OnCollisionEnter2D(Collision2D collision) {
        if(collision.gameObject.TryGetComponent<Player>(out Player player)) {
            player.Die();
            Die();
        }
    }
    public override void Die() {
        ScoreManager.Instance.AddScore(scorePoint);
        PlayerEnergy.Instance.Obtain(deathEnergyBonus);//玩家死亡前，直接将能量条增加
        EnemyManager.Instance.RemoveFromList(gameObject);//敌人死亡移除列表中的元素
        lootSpawner.Spawn(transform.position); 
        base.Die();
    }
    protected virtual void SetHealth() {
        maxHealth += (int)(EnemyManager.Instance.WaveNumber / healthFactor);
    }

}
