using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {
    [Header("==== MOVE ====")]
    //[SerializeField] float paddingX;
    //[SerializeField] float paddingY;
    [SerializeField] float moveSpeed = 2f;
    [SerializeField] float rotationAngle = 20f;

    [Header("==== FIRE ====")]
    [SerializeField] protected float minFireInterval;
    [SerializeField] protected float maxFireInterval;
    [SerializeField] protected GameObject[] projectiles;//子弹
    [SerializeField] protected Transform muzzle;//枪口
    [SerializeField] protected ParticleSystem muzzleVFX;//枪口特效
    [SerializeField] protected AudioData[] projectileLaunchSFX;//子弹发射音效

    protected float paddingX;
    protected float paddingY;
    protected Vector3 targetPosition;

    WaitForFixedUpdate waitForFixedUpdate = new WaitForFixedUpdate();

    protected virtual void Awake() {
        var size = transform.GetChild(0).GetComponent<Renderer>().bounds.size;//模型大小
        paddingX = size.x / 2f;
        paddingY = size.y / 2f;
    }
    protected virtual void OnEnable() {

        StartCoroutine(nameof(RandomlyMovingCoroutine));
        StartCoroutine(nameof(RandomFireCoroutine));

    }
    private void OnDisable() {
        StopAllCoroutines();//直接停止这个脚本的所有协程
    }
    IEnumerator RandomlyMovingCoroutine() {
        transform.position = Viewport.Instance.RandomEnemySpawnPosition(paddingX, paddingY);
        targetPosition = Viewport.Instance.RandomRightHalfPosition(paddingX, paddingY);

        while (gameObject.activeSelf) {//死循环后，编译器会进不去，一直卡在Hold on 
            //Mathf.Epsilon 是接近于0但是不等于0的数
            if (Vector3.Distance(transform.position, targetPosition) >= moveSpeed * Time.fixedDeltaTime) {
                //目标持续接近目标
                transform.position = Vector3.MoveTowards(transform.position, targetPosition, moveSpeed * Time.fixedDeltaTime);
                //按照某一个方向来旋转
                transform.rotation = Quaternion.AngleAxis((targetPosition - transform.position).normalized.y * rotationAngle, Vector3.right);
            }
            //新的位置
            else targetPosition = Viewport.Instance.RandomRightHalfPosition(paddingX, paddingY);
            yield return waitForFixedUpdate;
        }
    }
    protected virtual IEnumerator RandomFireCoroutine() {
        while (gameObject.activeSelf) {
            //完全使用随机间隔
            yield return new WaitForSeconds(Random.Range(minFireInterval, maxFireInterval));
            if (GameManager.GameState == GameState.GameOver) yield break;//直接停止协程
            foreach (var projectile in projectiles) {//子弹发射
                PoolManager.Release(projectile, muzzle.position);
            }
            AudioManager.Instance.PlayRandomSFX(projectileLaunchSFX);//子弹发射音效
            muzzleVFX.Play();
        }
    }
}
