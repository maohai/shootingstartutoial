using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossController : EnemyController
{
    [SerializeField] float continuousFireDuration = 1.5f;
    List<GameObject> magazine;//弹夹
    AudioData launchSFX;//临时开火音效
    [Header("---- Player Detection ----")]
    [SerializeField] Transform playerDetectionTransform;//检测盒的变换组件
    [SerializeField] Vector3 playerDetectionsSize;//检测盒的大小
    [SerializeField] LayerMask playerLayer;//检测盒的层级

    [Header("---- Beam ----")]
    [SerializeField] float beamCooldownTime = 12f;//激光武器冷却时间
    [SerializeField] AudioData beamChargingSFX;
    [SerializeField] AudioData beamLaunchSFX;

    bool isBeamReady;//是否冷却完毕
    int launchBeamID = Animator.StringToHash("launchBeam");
    Animator animator;
    Transform playerTransform;

    WaitForSeconds waitForContinuousFireInterval;
    WaitForSeconds waitforFireInterval;
    WaitForSeconds waitBeamCooldownTime;

    protected override void Awake() {
        base.Awake();
        animator = GetComponent<Animator>();
        waitForContinuousFireInterval = new WaitForSeconds(minFireInterval);//最小开火间隔,就是没次minFireInterval发射一次子弹
        waitforFireInterval = new WaitForSeconds(maxFireInterval);//每波开火间隔，连续发射子弹之后停留maxFireInterval继续发射
        waitBeamCooldownTime = new WaitForSeconds(beamCooldownTime);

        magazine = new List<GameObject>(projectiles.Length);
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
    }
    protected override void OnEnable() {
        //boss一出场，激光武器就会被冷却
        isBeamReady = false;
        muzzleVFX.Stop(); 
        StartCoroutine(nameof(BeamCooldownCoroutine));
        base.OnEnable();
    }
    void ActivateBeamWeapon() {
        isBeamReady = false;//重置冷却
        animator.SetTrigger(launchBeamID);//开启动画
        AudioManager.Instance.PlayRandomSFX(beamChargingSFX);//播放音效
    }
    void AnimationEventLaunchBeam() {
        AudioManager.Instance.PlayRandomSFX(beamLaunchSFX);
    }
    void AnimationEventStopBeam() {
        StopCoroutine(nameof(ChasingPlayerCoroutine));//停用追击玩家协程，正常随机移动
        StartCoroutine(nameof(BeamCooldownCoroutine));//开启冷却时间
        StartCoroutine(nameof(RandomFireCoroutine));//开启开火协程
    }
    void LoadProjectiles() {//装填子弹
        magazine.Clear();
        //使用物理检测
        if (Physics2D.OverlapBox(playerDetectionTransform.position,playerDetectionsSize,0f,playerLayer)) {
            //当玩家在boss正前方的时候   发射一号种类的子弹
            magazine.Add(projectiles[0]);//跟projectiles序列化排列有关
            launchSFX = projectileLaunchSFX[0];
        }
        else {
            //否则发射2||3号种类
            if (Random.value < 0.5f) {
                magazine.Add(projectiles[1]);
                launchSFX = projectileLaunchSFX[1];
            }
            else {
                for (int i = 2 ; i < projectiles.Length; ++i) 
                    magazine.Add(projectiles[i]);
                launchSFX = projectileLaunchSFX[2];
            }
        }
    }
    private void OnDrawGizmosSelected() {//用于debug
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(playerDetectionTransform.position, playerDetectionsSize);
    }
    protected override IEnumerator RandomFireCoroutine() {
        while (isActiveAndEnabled) {
            if (GameManager.GameState == GameState.GameOver) yield break;
            if (isBeamReady) {
                ActivateBeamWeapon();
                StartCoroutine(nameof(ChasingPlayerCoroutine));//boss在蓄力的时候，就会一遍往右一遍追击玩家
                yield break;
            }
            yield return waitforFireInterval;
            yield return StartCoroutine(nameof(ContinuousFireCoroutine));
        }
    }
    IEnumerator ContinuousFireCoroutine() {
        LoadProjectiles();
        muzzleVFX.Play();

        float continuousFireTimer = 0f;
        while(continuousFireTimer < continuousFireDuration) {
            foreach (var projectile in magazine)
                PoolManager.Release(projectile, muzzle.position);
            continuousFireTimer += minFireInterval;//每次开火都会增加一点时间
            AudioManager.Instance.PlayRandomSFX(launchSFX);

            yield return waitForContinuousFireInterval;
        }
        muzzleVFX.Stop();
    }
    IEnumerator BeamCooldownCoroutine() {
        yield return waitBeamCooldownTime;

        isBeamReady = true;
    }
    IEnumerator ChasingPlayerCoroutine() {
        //追击玩家
        while (isActiveAndEnabled) {
            targetPosition.x = Viewport.Instance.MaxX - paddingX;
            targetPosition.y = playerTransform.position.y;

            yield return null;
        }
    }
}
