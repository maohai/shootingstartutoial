using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///
/// </summary>

public class PlayerMissile : PlayerProjectileOverdrive
{
    [SerializeField] AudioData targetAcquiredVoice = null;
    [Header("=== SPEED CHANGE ===")]
    [SerializeField] float lowSpeed = 8f;
    [SerializeField] float highSpeed = 25f;
    [SerializeField] float variableSpeedDelay = 0.5f;//慢速持续时间
    [Header("=== EXPLOSION")]
    [SerializeField] GameObject explosionVFX = null;
    [SerializeField] AudioData explosionSFX = null;
    [SerializeField] float explosionRadius = 3f;//爆炸半径
    [SerializeField] LayerMask enemyLayerMask = default;//层级遮罩
    [SerializeField] float explosionDamage = 100f;//爆炸伤害

    //[SerializeField] Collider2D explosionCollider;

    WaitForSeconds waitVariableSpeedDelay;

    protected override void Awake() {
        base.Awake();
        waitVariableSpeedDelay = new WaitForSeconds(variableSpeedDelay);

        //导弹发射的时候禁用这个碰撞体
        //explosionCollider.enabled = false;
    }
   
    protected override void OnCollisionEnter2D(Collision2D collision) {
        base.OnCollisionEnter2D(collision);
        PoolManager.Release(explosionVFX, transform.position);
        AudioManager.Instance.PlayRandomSFX(explosionSFX);
        //碰到的时候启用检测
        //explosionCollider.enabled = true;
        //DistanceDetection();
        
        //返回范围类所有的碰撞器，(中心点，半径，层级遮罩)
        var colliders = Physics2D.OverlapCircleAll(transform.position, explosionRadius, enemyLayerMask);
        foreach (var collider in colliders) {
            if (collider.TryGetComponent<Enemy>(out Enemy enemy))
                enemy.TakeDamage(explosionDamage);  
        }
    }
    //爆炸范围
    private void OnDrawGizmosSelected() {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, explosionRadius);
    }
    protected override void OnEnable() {
        base.OnEnable();
        StartCoroutine(nameof(VariableSpeedCoroutine));
    }
    
    //private void OnTriggerEnter2D(Collider2D collision) {
    //    //添加一个触发器，当检测到敌人进入触发器范围时，敌人收到伤害
    //    if (collision.TryGetComponent<Enemy>(out Enemy enemy))
    //        enemy.TakeDamage(100f);
    //}
    IEnumerator VariableSpeedCoroutine() {
        moveSpeed = lowSpeed;
        yield return waitVariableSpeedDelay;
        moveSpeed = highSpeed;
        if (target != null)
            AudioManager.Instance.PlayRandomSFX(targetAcquiredVoice);
    }
    //void DistanceDetection() {
    //    //遍历所有的敌人
    //    foreach (var enemyInRange in EnemyManager.Instance.Enemies)
    //        //如果在范围内
    //        if (Vector2.Distance(transform.position, enemyInRange.transform.postion) <= 3f)
    //            if (enemyInRange.TryGetComponent<Enemy>(out Enemy enemy))
    //                enemy.TakeDamage(100);
    //}
}
