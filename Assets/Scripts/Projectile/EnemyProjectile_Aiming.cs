using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///
/// </summary>

public class EnemyProjectile_Aiming : Projectile
{
    private void Awake() {
        SetTarget(GameObject.FindGameObjectWithTag("Player"));//通过对象标签来查找物体
    }
    protected override void OnEnable() {
        StartCoroutine(nameof(MoveDirectionCoroutine));
        base.OnEnable();//如果玩家死亡，这一行代码无法正常工作
    }
    IEnumerator MoveDirectionCoroutine() {
        //在子弹被启用的瞬间，考虑到浮点数的精确度，所以应该让引擎在下一帧获取精确的值
        yield return null;
        if (target.activeSelf)
            moveDirectly = (target.transform.position - transform.position).normalized;
    }
}
