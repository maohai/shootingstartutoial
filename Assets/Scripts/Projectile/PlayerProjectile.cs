using UnityEngine;

public class PlayerProjectile : Projectile
{
    TrailRenderer trail;//轨迹

    protected virtual void Awake() {
        //因为轨迹组件是在子对象上
        trail = GetComponentInChildren<TrailRenderer>();
        if (moveDirectly != Vector2.right) {
            transform.GetChild(0).rotation = Quaternion.FromToRotation(Vector2.right, moveDirectly);
        }
    }
    private void OnDisable() {
        //当子弹消失的时候清除轨迹
        trail.Clear();
    }
    protected override void OnCollisionEnter2D(Collision2D collision) {
        base.OnCollisionEnter2D(collision);
        //表示玩家每一颗子弹命中的时候，都会增加一点能量条
        PlayerEnergy.Instance.Obtain(PlayerEnergy.PERSENT);
    }
}
