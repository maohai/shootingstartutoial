using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] GameObject hitFVXZ;//命中视觉特效
    [SerializeField] float damage;//伤害
    [SerializeField] protected float moveSpeed = 10f;//速度
    [SerializeField] protected Vector2 moveDirectly;//子弹方向
    [SerializeField] AudioData[] hitSFX;//子弹命中音效
    protected GameObject target;//子弹目标
    protected virtual void OnEnable() {
        //每一次游戏对象启用的时候执行 
        StartCoroutine(MoveDirectly());
    }
   
    private IEnumerator MoveDirectly() {
        //当物体在启用条件的时候
        //只要当子弹没有被摧毁或者禁用，那么就会不断的移动
        while (gameObject.activeSelf) {
            //不依赖刚体组件，大大降低了游戏的性能
             Move();
             yield return null;
        }
    }

    protected virtual void OnCollisionEnter2D(Collision2D collision) {
        if(collision.gameObject.TryGetComponent<Character>(out Character character)) {
            //当抓取到返回类型的值的时候，返回true，消耗性能更小
            character.TakeDamage(damage);
            //返回的是一个contactPoint2D类型，表示的是两个碰撞体的接触点，参数0也就是第一个接触点
            var contactPoint = collision.GetContact(0);
            //物体，位置，旋转方向    Quaternion.LookRotation根据传入的前方以及上方返回一个旋转值
            PoolManager.Release(
                hitFVXZ, 
                contactPoint.point, 
                Quaternion.LookRotation(contactPoint.normal/*法线方向*/)
                );
            AudioManager.Instance.PlayRandomSFX(hitSFX);
            gameObject.SetActive(false);
        }

    }
    protected void SetTarget(GameObject target) => this.target = target;
    public void Move() => transform.Translate(moveDirectly.normalized * moveSpeed * Time.deltaTime);
}
