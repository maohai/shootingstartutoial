using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//传入自己，就变成单例模式了
//然后在其他类里，使用Viewport.Instance就可以取到实例了
public class Viewport : Singleton<Viewport>
{
    float minX;
    float maxX;
    float minY;
    float maxY;
    float midX;

    public float MaxX => maxX;
    private void Start() {
        //因为玩家位置是世界位置，所以必须通过ViewportToWorldPoint来转换
        Camera mainCamera = Camera.main;//将主摄像机赋值
        Vector2 bottomLeft = mainCamera.ViewportToWorldPoint(new Vector2(0, 0));//视口左下角
        Vector2 topRight = mainCamera.ViewportToWorldPoint(new Vector2(1, 1));//视口右上角

        midX = mainCamera.ViewportToWorldPoint(new Vector2(0.5f, 0)).x;
        minX = bottomLeft.x;
        minY = bottomLeft.y;
        maxX = topRight.x;
        maxY = topRight.y;
    }

    public Vector3 PlayerMoveablePosition(Vector3 playerPosition,float paddingX,float paddingY) {
        //Mathf.Clamp(value,min,max)可以将浮点数限定在一个范围内
        //如果value小于min，返回min，如果大于max，返回max
        playerPosition.x = Mathf.Clamp(playerPosition.x, minX + paddingX, maxX - paddingX);
        playerPosition.y = Mathf.Clamp(playerPosition.y, minY + paddingY, maxY - paddingY);
        //这样就得到了一个玩家限定的范围了

        return playerPosition;
    }


    public Vector3 RandomEnemySpawnPosition(float paddingX, float paddingY) {
        Vector3 position = Vector3.zero;
        position.x = maxX + paddingX;
        position.y = Random.Range(minY + paddingY, maxY - paddingY);

        return position;
    }
    public Vector3 RandomRightHalfPosition(float paddingX, float paddingY) {
        Vector3 position = Vector3.zero;
        position.x = Random.Range(midX, maxX - paddingX);
        position.y = Random.Range(minY + paddingY, maxY - paddingY);

        return position;
    }
    public Vector3 RandomEnemyMovePosition(float paddingX, float paddingY) {
        Vector3 position = Vector3.zero;
        position.x = Random.Range(minX, maxX - paddingX);
        position.y = Random.Range(minY + paddingY, maxY - paddingY);

        return position;
    }
}
