using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileSystem : MonoBehaviour
{
    [SerializeField] GameObject missilePrefab = null;
    [SerializeField] AudioData launchSFX = null;
    [SerializeField] int defaultAmount = 5;
    [SerializeField] float cooldownTime = 1f;
    bool isReady = true;
    int amount;

 
    private void Awake() {
        amount = defaultAmount;
        //waitCoolDownTime = new WaitForSeconds(cooldownTime);
    }
    private void Start() {
        MissileDisplay.UpdateAmountText(amount);
    }
    public void PickUp() {
        ++amount;
        MissileDisplay.UpdateAmountText(amount);
        if(amount == 1) {
            MissileDisplay.UpdateCooldownImage(0f);
            isReady = true;
        }
    }
    public void Launch(Transform muzzleTransform) {
        if (amount == 0 || !isReady) return;
        isReady = false;
        PoolManager.Release(missilePrefab, muzzleTransform.position);
        AudioManager.Instance.PlayRandomSFX(launchSFX);
        amount--;
        MissileDisplay.UpdateAmountText(amount);
        if (amount == 0) MissileDisplay.UpdateCooldownImage(1f);
        else StartCoroutine(nameof(CoolDownCoroutine));
    }
    IEnumerator CoolDownCoroutine() {
        var cooldownValue = cooldownTime;
        while (cooldownValue > 0f) {
            MissileDisplay.UpdateCooldownImage(cooldownValue / cooldownTime);
            cooldownValue = Mathf.Max(cooldownValue - Time.deltaTime, 0f);
            yield return null;
        }
        //yield return waitCoolDownTime;
        isReady = true;
    }
}
