using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileGuidanceSystem : MonoBehaviour
{
    [SerializeField] Projectile projectile;//子弹脚本
    [SerializeField] float minBallisticAngle = 50f;//最小偏转角
    [SerializeField] float maxBallisticAngle = 75f;//最大偏转
    float ballisticAngle;//当前子弹的偏转角度
    Vector3 targetDirection;
    public IEnumerator HomingCoroutine(GameObject target) {
        ballisticAngle = Random.Range(minBallisticAngle, maxBallisticAngle);
        while (gameObject.activeSelf) {
            if (target.activeSelf) {
                targetDirection = target.transform.position - transform.position;//不断朝向目标
                transform.rotation = //x轴偏转
                    Quaternion.AngleAxis(Mathf.Atan2(targetDirection.y, targetDirection.x) * Mathf.Rad2Deg, Vector3.forward);
                transform.rotation *= Quaternion.Euler(0, 0, ballisticAngle);//偏转
                projectile.Move();//正前方移动
            }
            else projectile.Move();
            yield return null;
        }
    }
}
