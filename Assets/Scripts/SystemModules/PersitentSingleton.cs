using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersitentSingleton<T> : 
    MonoBehaviour where T:Component//泛型参数，泛型约束
{
    public static T Instance { get;private set; }

    protected virtual void Awake() {
        if (Instance == null)
            Instance = this as T;
        else Destroy(this);//防止出现两个静态实例造成冲突

        //当游戏加载新的场景的时候，将会摧毁所有的游戏对象
        //DontDestroyOnLoad告诉游戏引擎不要摧毁传入的游戏对象
        DontDestroyOnLoad(gameObject);
    }
}
