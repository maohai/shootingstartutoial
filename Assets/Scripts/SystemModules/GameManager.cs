using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : PersitentSingleton<GameManager> {
    public static System.Action onGameOver;
    // 得到一个封装了的静态属性
    public static GameState GameState { get => Instance.gameState; set => Instance.gameState = value; }
    [SerializeField] GameState gameState = GameState.Playing;
}

public enum GameState {
    Playing,
    Paused,
    GameOver,
    Scoring,
}