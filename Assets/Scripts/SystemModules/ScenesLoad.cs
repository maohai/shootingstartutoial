using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;//使用场景加载器
using UnityEngine.UI;
public class ScenesLoad : PersitentSingleton<ScenesLoad> {

    [SerializeField] Image transitionImage;
    [SerializeField] float fadeTime = 3.5f;

    Color color;
    const string GAMEPLAY = "Gameplay";
    const string MAINMENU = "Main Menu";
    const string SCORING = "Scoring";
    IEnumerator LoadingCoroutine(string sceneName) {
        //返回的是一个AsyncOperation异步操作类，可以用来判断加载是否完成
        var loadingOperation = SceneManager.LoadSceneAsync(sceneName);//防止卡顿
        loadingOperation.allowSceneActivation = false;//允许场景激活设置为false，使用来设置加载好的场景是否为激活状态
        transitionImage.gameObject.SetActive(true);
        //画面淡出
        while (color.a < 1f) {
            //Time.unscaledDeltaTime影响，所以当游戏暂停的时候也会继续执行
            color.a = Mathf.Clamp01(color.a + Time.unscaledDeltaTime / fadeTime);//限制在01之间
            transitionImage.color = color;
            yield return null;
        }
        //当异步加载操作的进度值大于或者等于0.9的时候才会激活新的场景
        yield return new WaitUntil(() => loadingOperation.progress >= 0.9f);
        //加载场景
        loadingOperation.allowSceneActivation = true;//设置为true，显示异步加载好的场景
        //画面淡入
        while (color.a > 0f) {
            //Time.unscaledTime不受Time.timeScale影响，所以当游戏暂停的时候也会继续执行
            color.a = Mathf.Clamp01(color.a - Time.unscaledDeltaTime / fadeTime);//限制在01之间
            transitionImage.color = color;
            yield return null;
        }
        transitionImage.gameObject.SetActive(false);
    }

    internal void LoadScoringScene() {
        StopAllCoroutines();//频繁的切换场景的时候防止出现问题
        StartCoroutine(LoadingCoroutine(SCORING));
    }

    public void LoadGameplayScene() {
        StopAllCoroutines();//频繁的切换场景的时候防止出现问题
        StartCoroutine(LoadingCoroutine(GAMEPLAY));
    }
    public void LoadMainMenuScene() {
        StopAllCoroutines();//防止出现问题
        StartCoroutine(LoadingCoroutine(MAINMENU));
    }
}
