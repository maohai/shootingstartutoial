using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///
/// </summary>

public class TimeController : Singleton<TimeController> {
    [SerializeField, Range(0f, 1f)] float bulletTimeScale = 0.1f;
    float defaultFixedDeltaTime;
    float timeScaleBeforePause;
    float t;
    protected override void Awake() {
        base.Awake();
        defaultFixedDeltaTime = Time.deltaTime;
    }
    public void Pause() {
        timeScaleBeforePause = Time.timeScale;
        Time.timeScale = 0f;
    }
    public void Unpause() {
        Time.timeScale = timeScaleBeforePause;
    }
    public void BulletTime(float duration) {
        Time.timeScale = bulletTimeScale;
        StartCoroutine(SlowOutCoroutine(duration));
    }
    public void BulletTime(float inDuration, float outDuration) {
        StartCoroutine(SlowInAndOutCoroutine(inDuration, outDuration));
    }
    public void BulletTime(float inDuration, float keepingDuration, float outDuration) {
        StartCoroutine(SlowInKeepAndOutDuration(inDuration, keepingDuration, outDuration));
    }
    public void SlowIn(float duration) {
        StartCoroutine(SlowInCoroutine(duration));
    }
    public void SlowOut(float duration) {
        StartCoroutine(SlowOutCoroutine(duration));
    }
    //时间由快到慢
    IEnumerator SlowOutCoroutine(float duration) {
        t = 0f;
        while (t < 1f) {
            if (GameManager.GameState != GameState.Paused) {
                //不受时间刻度的影响，防止时间被拉长，如果是deltaTime，则实际被拉长被2.4s左右
                t += Time.unscaledDeltaTime / duration;
                Time.timeScale = Mathf.Lerp(bulletTimeScale, 1f, t);
                Time.fixedDeltaTime = defaultFixedDeltaTime * Time.timeScale;//防止卡顿
            }

            yield return null;
        }
    }
    //时间由慢到快
    IEnumerator SlowInCoroutine(float duration) {
        t = 0f;
        while (t < 1f) {
            if (GameManager.GameState != GameState.Paused) {
                t += Time.unscaledDeltaTime / duration;
                Time.timeScale = Mathf.Lerp(1f, bulletTimeScale, t);
                Time.fixedDeltaTime = defaultFixedDeltaTime * Time.timeScale;//防止卡顿
            }

            yield return null;
        }
    }
    //时间由快-慢-快
    IEnumerator SlowInAndOutCoroutine(float inDuration, float outDuration) {
        yield return StartCoroutine(SlowInCoroutine(inDuration));
        StartCoroutine(SlowOutCoroutine(outDuration));
    }
    //时间由快-慢-保持一段时间-快
    IEnumerator SlowInKeepAndOutDuration(float inDuration, float keepingDuration, float outDuration) {
        yield return StartCoroutine(SlowInCoroutine(inDuration));
        yield return new WaitForSecondsRealtime(keepingDuration);//不受时间刻度影响
        StartCoroutine(SlowOutCoroutine(outDuration));
    }
}
