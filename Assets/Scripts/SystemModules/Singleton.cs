using UnityEngine;

//泛型类，因为component是能挂载到所有游戏对象的基类，所以T的类型参数为Component
public class Singleton<T> : MonoBehaviour where T:Component {
    public static T Instance { get; private set; }

    protected virtual void Awake() {//能够让继承他的类重写这个函数
        Instance = this as T;//将this转为泛型T
    }
}
