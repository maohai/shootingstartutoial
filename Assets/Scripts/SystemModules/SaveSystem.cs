using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;//用于数据存储

namespace SaveSystemTutorial {
    public static class SaveSystem {
        #region PlayerPrefs
        public static void SaveByPlayerPrefs(string key, object data) {
            //var json = JsonUtility.ToJson(data,true);//第二个参数转为更为适合阅读的标准JSON格式
            var json = JsonUtility.ToJson(data, true);

            PlayerPrefs.SetString(key, json);
            PlayerPrefs.Save();
        }
        public static string LoadFromPlayerPrefs(string key) {
            return PlayerPrefs.GetString(key, null);
        }
        #endregion
        #region JSON
        public static void SaveByJson(string saveFileName, object data) {
            var json = JsonUtility.ToJson(data);
            //Application.persistentDataPath  用于提供一个存储永久数据的路径
            var path = Path.Combine(Application.persistentDataPath, saveFileName);//合并路径
            try {
                File.WriteAllText(path, json);
#if UNITY_EDITOR
                Debug.Log($"successfully saved data to {path}");
#endif
            }
            catch (System.Exception exception) {
#if UNITY_EDITOR
                Debug.Log($"failed to saved data to {path}.\n{exception}");
#endif
            }
        }
        public static T LoadFromJson<T>(string saveFileName) {
            var path = Path.Combine(Application.persistentDataPath, saveFileName);//合并路径

            try {
                var json = File.ReadAllText(path);
                var data = JsonUtility.FromJson<T>(json);
#if UNITY_EDITOR
                return data;
#endif
            }
            catch (System.Exception exception) {
#if UNITY_EDITOR
                Debug.Log($"failed to load data to {path}.\n{exception}");
#endif
                return default;
            }

        }
        #endregion
        #region Deletion
        public static void DeleteSaveFile(string saveFileName) {
            var path = Path.Combine(Application.persistentDataPath,saveFileName);

          
            try {
                File.Delete(path);
            }
            catch (System.Exception exception) {
#if UNITY_EDITOR
                Debug.Log($"failed to delete data to {path}.\n{exception}");
#endif
            }
        }
        #endregion
        public static bool SaveFileExists(string saveFileName) {
            //判断文件是否存在
            var path = Path.Combine(Application.persistentDataPath, saveFileName);
            return File.Exists(path);
        }
    }
   
}

