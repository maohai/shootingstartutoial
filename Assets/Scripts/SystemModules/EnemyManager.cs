using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : Singleton<EnemyManager>
{
    public GameObject RandomEnemy => enemyList.Count == 0 ? null : enemyList[Random.Range(0, enemyList.Count)];//随机返回一个敌人
    public int WaveNumber => waveNumber;//用于外部获取
    public float TimeBetweenWaves => timeBetweenWaves;//用于外部获取

    [SerializeField] GameObject waveUI;//获取面板
    [SerializeField] bool spawnEnemy = true;//开启生成下一波
    [SerializeField] GameObject[] enemyPrefabs;//敌人数组
    [SerializeField] float timeBetweenSpawn = 1f;//生成敌人间隔时间
    [SerializeField] float timeBetweenWaves = 1f;//等待下一波生成的时间
    [SerializeField] int minEnemyAmout = 4;//最小敌人数量
    [SerializeField] int maxEnemyAmout = 10;//最大敌人数量
    int waveNumber = 1;//敌人波数
    int enemyAmout;//敌人数量
    List<GameObject> enemyList;//敌人列表，用于判断新生成的敌人全部死亡
    [Header("--- Boss Setting ----")]
    [SerializeField] GameObject bossPrefab;//敌人预制体
    [SerializeField] int bossWaveNumber = 3;//波数间隔

    WaitForSeconds waitTimeBetweenSpawns;//等待生成间隔
    WaitForSeconds waitTimeBetweenWaves;//等待每波间隔时间
    WaitUntil waitUntilNoEnemy;//挂起等待，直到条件满足

    protected override void Awake() {
        base.Awake();//也就是执行Instance = this as EnemyManager;
        enemyList = new List<GameObject>();
        waitTimeBetweenSpawns = new WaitForSeconds(timeBetweenSpawn);
        waitTimeBetweenWaves = new WaitForSeconds(timeBetweenWaves);
        waitUntilNoEnemy = new WaitUntil(() => enemyList.Count == 0);//参数为委托,也就是函数
    }

    private IEnumerator Start() {
        while (spawnEnemy && GameManager.GameState != GameState.GameOver) {
            waveUI.SetActive(true);//启用UI
            yield return waitTimeBetweenWaves;//下一波
            waveUI.SetActive(false);//禁用UI
            yield return StartCoroutine(nameof(RandomlySpawnCoroutine));//协程嵌套
        }
    }
    private IEnumerator RandomlySpawnCoroutine() {
        if (waveNumber % bossWaveNumber == 0) {
            var boss = PoolManager.Release(bossPrefab);
            enemyList.Add(boss);
        }
        else {
            enemyAmout = Mathf.Clamp(enemyAmout, minEnemyAmout + waveNumber / bossWaveNumber, maxEnemyAmout);
            for (int i = 0; i < enemyAmout; i++) {
                //随机释放数组中的敌人
                enemyList.Add(PoolManager.Release(enemyPrefabs[Random.Range(0, enemyPrefabs.Length)]));
                yield return waitTimeBetweenSpawns;
            }
        }
        //第二个参数 随着敌人波数的增加而增加
        yield return waitUntilNoEnemy;//等待条件满足
        waveNumber++;
    }
    public void RemoveFromList(GameObject enemy) => enemyList.Remove(enemy);//lambda表达式
}
