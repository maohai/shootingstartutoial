using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : PersitentSingleton<ScoreManager>
{
    #region SCORE DISPLAY
    public int Score => score;
    int score;
    int currentScore;
    Vector3 scoreScaleText = new Vector3(1.2f, 1.2f, 1f);
    public void ResetScore() {
        score = 0;
        currentScore = 0;
        ScoreDisplay.UpdateText(score);
    }
    public void AddScore(int scorePoint) {
        currentScore += scorePoint;
        StartCoroutine(nameof(AddScoreCoroutine));
    }
    IEnumerator AddScoreCoroutine() {
        ScoreDisplay.ScaleText(scoreScaleText);
        while(score< currentScore) {
            score += 1;
            ScoreDisplay.UpdateText(score);
            yield return null;
        }
        ScoreDisplay.ScaleText(Vector3.one);

    }
    #endregion
    #region HIGH SCORE SYSTEM
    [System.Serializable] public class PlayerScore {
        //序列化一个类
        public int score;
        public string playerName;
        public PlayerScore(int score,string playerName) {
            this.score = score;
            this.playerName = playerName;
        }
    }
    [System.Serializable] public class PlayerScoreData {
        //最终要存储JSON的数据格式
        public List<PlayerScore> list = new List<PlayerScore>();
    }
    readonly string SaveFileName = "player_score.json";
    string playerName = "gou dan";
    public bool HasNewHighScore => score > LoadPlayerScoreData().list[9].score;
    public void SetPlayerName(string newName) => playerName = newName;
    public void SavePlayerScoreData() {
        //数据存档
        var playerScoreData = LoadPlayerScoreData();

        playerScoreData.list.Add(new PlayerScore(score, playerName));
        playerScoreData.list.Sort((x, y) => y.score.CompareTo(x.score));//降序

        SaveSystemTutorial.SaveSystem.SaveByJson(SaveFileName, playerScoreData);
    }
    public PlayerScoreData LoadPlayerScoreData() {
        //读取得分列表
        var playerScoreData = new PlayerScoreData();
        if(SaveSystemTutorial.SaveSystem.SaveFileExists(SaveFileName))
            playerScoreData = SaveSystemTutorial.SaveSystem.LoadFromJson<PlayerScoreData>(SaveFileName);
        else {
            while (playerScoreData.list.Count < 10)
                playerScoreData.list.Add(new PlayerScore(0, playerName));
            SaveSystemTutorial.SaveSystem.SaveByJson(SaveFileName, playerScoreData);
        }
        return playerScoreData;
    }
    #endregion
}
