using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour {
    [SerializeField] Pool[] EnemyPools;//敌人池子
    [SerializeField] Pool[] playerProjectilePools;//主角对象池数组
    [SerializeField] Pool[] enemyProjectilePools;//敌人子弹对象池数组
    [SerializeField] Pool[] vFXPools;//特效池子
    [SerializeField] Pool[] lootItemPools;//战利品池子


    //Pool就是一个对象池
    static Dictionary<GameObject, Pool> dictionary;
    private void Awake() {//避免脚本执行顺序随机化而引发的bug
        dictionary = new Dictionary<GameObject, Pool>();
        Initialize(EnemyPools);
        Initialize(playerProjectilePools);
        Initialize(enemyProjectilePools);
        Initialize(vFXPools);
        Initialize(lootItemPools);
    }
#if UNITY_EDITOR
    private void OnDestroy() {//会在游戏编辑器游戏停止时候自动调用
        CheckPoolSize(EnemyPools);
        CheckPoolSize(playerProjectilePools);
        CheckPoolSize(enemyProjectilePools);
        CheckPoolSize(vFXPools);
        CheckPoolSize(lootItemPools); 
    }
#endif
    void CheckPoolSize(Pool[] pools) {//发出警告
        foreach (var pool in pools) {
            if (pool.RuntimeSize > pool.Size)
                Debug.LogWarning(
                    string.Format("Pool:{0} has a runtime size {1} bigger than its initial size {2}",
                    pool.Prefab.name,
                    pool.RuntimeSize,
                    pool.Size)
                    );
        }
    }
    void Initialize(Pool[] pools) {
        foreach (var pool in pools) {
#if UNITY_EDITOR //只有unity会报错，其他平台不得行，帮助debug用
            if (dictionary.ContainsKey(pool.Prefab)) {
                Debug.LogError("存在相同的预制体" + pool.Prefab.name);
                continue;//存在相同预制体
            }
#endif
            dictionary.Add(pool.Prefab, pool);//key不同，值相同
            Transform poolParent = new GameObject("Pool:" + pool.Prefab.name).transform;
            poolParent.parent = transform;
            pool.Initialize(poolParent);
        }
    }

    #region RELEASE
    public static GameObject Release(GameObject prefab) {//释放出一个预制体
#if UNITY_EDITOR
        if (!dictionary.ContainsKey(prefab)) {
            Debug.LogError("找不到" + prefab.name);
            return null;
        }
#endif
        //将对应的池中预制件取出
        return dictionary[prefab].PrepareObject();
    }
    public static GameObject Release(GameObject prefab, Vector3 pos) {//释放出一个预制体
#if UNITY_EDITOR
        if (!dictionary.ContainsKey(prefab)) {
            Debug.LogError("找不到" + prefab.name);
            return null;
        }
#endif
        //将对应的池中预制件取出
        return dictionary[prefab].PrepareObject(pos);
    }
    public static GameObject Release(GameObject prefab, Vector3 pos, Quaternion rotation) {//释放出一个预制体
#if UNITY_EDITOR
        if (!dictionary.ContainsKey(prefab)) {
            Debug.LogError("找不到" + prefab.name);
            return null;
        }
#endif
        //将对应的池中预制件取出
        return dictionary[prefab].PrepareObject(pos, rotation);
    }
    public static GameObject Release(GameObject prefab, Vector3 pos, Quaternion rotation, Vector3 localScale) {//释放出一个预制体
#if UNITY_EDITOR
        if (!dictionary.ContainsKey(prefab)) {
            Debug.LogError("找不到" + prefab.name);
            return null;
        }
#endif
        //将对应的池中预制件取出
        return dictionary[prefab].PrepareObject(pos, rotation, localScale);
    }
    #endregion

}
