using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//因为没有继承Mono类，所以得添加命名空间
//这样才能正确暴露

[System.Serializable]
public class Pool
{
    public int Size => size;//对象池尺寸
    public int RuntimeSize => queue.Count;//对象池实际尺寸

    //public GameObject Prefab{get=>prefab;};
    public GameObject Prefab => prefab;
    [SerializeField]GameObject prefab;
    [SerializeField]int size = 1;
    Queue<GameObject> queue;

    Transform parent;
    public void Initialize(Transform parent) {
        queue = new Queue<GameObject>();
        this.parent = parent;
        for (int i = 0; i < size; i++) {
            //最末尾添加一个元素
            queue.Enqueue(Copy());
        }
    }
    GameObject Copy() {//创建对象
        GameObject copy = GameObject.Instantiate(prefab, parent);
        copy.SetActive(false);
        return copy;
    }
    GameObject AvailableObject() {//取出可用的对象
        GameObject availableObject = null;

        //当对象个数大于0，且第一个不是启用的时候
        if (queue.Count > 0 && !queue.Peek().activeSelf)
            //取出最前面的元素并删除队列里的
            availableObject = queue.Dequeue();
        else availableObject = Copy();

        //返回可用的对象之前直接入列
        queue.Enqueue(availableObject);

        return availableObject;
    }

    //将未启用的对象返回
    public GameObject PrepareObject() {
        GameObject prepareObject = AvailableObject();

        prepareObject.SetActive(true);

        return prepareObject;
    }
    public GameObject PrepareObject(Vector3 pos) {
        GameObject prepareObject = AvailableObject();

        prepareObject.SetActive(true);
        prepareObject.transform.position = pos;

        return prepareObject;
    }
    public GameObject PrepareObject(Vector3 pos,Quaternion rotation) {
        GameObject prepareObject = AvailableObject();

        prepareObject.SetActive(true);
        prepareObject.transform.position = pos;
        prepareObject.transform.rotation = rotation;

        return prepareObject;
    }
    public GameObject PrepareObject(Vector3 pos, Quaternion rotation,Vector3 localScale) {
        GameObject prepareObject = AvailableObject();

        prepareObject.SetActive(true);
        prepareObject.transform.position = pos;
        prepareObject.transform.rotation = rotation;
        prepareObject.transform.localScale = localScale;

        return prepareObject;
    }

}
